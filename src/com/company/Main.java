package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<Integer> myList = new ArrayList<>(100);
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            myList.add(random.nextInt());
        }

        List<String> strings =  myList.stream().filter(list -> list%5 == 0).sorted().map(list-> "Value: " +list).collect(Collectors.toList());
        strings.forEach(string -> System.out.println(string));
    }
}
